## *Torulaspora microellipsoides* CLIB 830

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB7632](https://www.ebi.ac.uk/ena/browser/view/PRJEB7632)
* **Assembly accession**: [GCA_900186055.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900186055.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: TOMI
* **Assembly length**: 10,414,811
* **#Scaffolds**: 46
* **Mitochondiral**: No
* **N50 (L50)**: 1,189,108 (4)

### Annotation overview

**No annotation available**
